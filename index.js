console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


// For Personal Infromation

	let firstname = "Joram";
	console.log("First Name: " + firstname);

	let lastname = "Ape";
	console.log("Last Name: " + lastname);

	let Age = 30;
	console.log("My current age is: " + Age);

	let hobbies = "Hobbies:";
	console.log(hobbies);

	let hob = ["guitar","biking", "Swimming"]; // array types
	console.log(hob);

	let workaddress = "work address:";
	console.log(workaddress);

	let address = {
		houseNumber: '39',
		street: "apple",       // use object types
		city: "Bacolod city",
		state:"Philippines"
	}

	console.log(address);



// for debugging [sECTION]

	let fullName = "Steve Rogers";
	console.log("My full name is" + fullName);

	let age = 40;  // numbers type
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"]; // array types
	console.log("My Friends are: " + friends);
	console.log(friends);

	let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",  // object types
		age: 40,
		isActive: false

	}
	console.log("My Full Profile:");
	console.log(profile);

	let fullname = "Bucky Barnes";
	console.log("My bestfriend is: " + fullname);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

